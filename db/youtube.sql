-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 03, 2021 at 06:43 PM
-- Server version: 10.3.31-MariaDB-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `youtube-s`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `album_cnt`
-- (See below for the actual view)
--
CREATE TABLE `album_cnt` (
`album` varchar(255)
,`cnt` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `author_cnt`
-- (See below for the actual view)
--
CREATE TABLE `author_cnt` (
`author` varchar(255)
,`cnt` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `channels`
--

CREATE TABLE `channels` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `channels`
--

INSERT INTO `channels` (`id`, `name`, `youtube_id`) VALUES
(1, '美国之音中文网', 'UCt5zpwa264A0B-gaYtv1IpA'),
(2, 'BBC News 中文', 'UCb3TZ4SD_Ys3j4z0-8o6auA'),
(3, '文昭談古論今 - Wen Zhao Officia', 'UCtAIPjABiQD3qjlEl1T5VpA'),
(4, '江峰时刻', 'UCa6ERCDt3GzkvLye32ar89w'),
(5, '民視讚夯 Formosa TV Thumbs Up', 'UCS3nuaXXw7nsiSzdJ5HJaoQ'),
(6, '洛杉矶华人资讯网How视频', 'UC-ayKOXvIcatt5VocwTrU9Q'),
(7, '天亮時分', 'UCjvjNeHndz4PGs9JXhzdHqw'),
(8, '王剑每日观察Kim\'s Observation', 'UC8UCbiPrm2zN9nZHKdTevZA'),
(9, '明鏡火拍', 'UCdKyM0XmuvQrD0o5TNhUtkQ'),
(10, '阿波羅新聞網 - 熱點直擊', 'UCGf98FJIiU5mPSyAYG99NAQ'),
(11, '公子時評', 'UCrGSFNEBmCN0rqhATZels2Q'),
(12, '華爾街電視', 'UCs7elJEjfHfLUEZ7HUbUlUw'),
(13, '希望之聲TV', 'UCk89pEd76qutMB08hVSY49Q'),
(14, '真观点', 'UCAwVpzgGI9sEu4O4ZlB5ZWQ'),
(15, '老北京茶馆', 'UCpj_AT6Bt1_VT038hmfFGSQ'),
(16, '明镜新闻台', 'UC3lyWHqUY9IiP4en5jnY6vA'),
(17, '大康有话说', 'UCG6ADYIl4GxaQib1HKIsRNg'),
(18, 'DW 中文 - 德國之聲', 'UC99XkAM3Y82ONvdpwUYmOZg'),
(19, 'LT視界', 'UCOsQMj_MZkQ5N7f1OOMB87Q');

-- --------------------------------------------------------

--
-- Stand-in structure for view `download-error`
-- (See below for the actual view)
--
CREATE TABLE `download-error` (
`id` int(10) unsigned
,`channel_name` varchar(255)
,`yt_id` varchar(255)
,`title` varchar(255)
,`publish_date` datetime
,`thumbnail_url` varchar(255)
,`album` varchar(255)
,`author` varchar(255)
,`status` varchar(255)
,`created` timestamp
,`download_status` varchar(255)
,`download_started` datetime
,`download_completed` datetime
,`transcode_status` varchar(255)
,`transcode_started` datetime
,`transcode_completed` datetime
,`metaupdate_status` varchar(255)
,`metaupdate_started` datetime
,`metaupdate_completed` datetime
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `download_time`
-- (See below for the actual view)
--
CREATE TABLE `download_time` (
`yt_id` varchar(255)
,`author` varchar(255)
,`title` varchar(255)
,`download_time` bigint(16)
,`download_completed` datetime
);

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE `programs` (
  `id` int(10) UNSIGNED NOT NULL,
  `channel_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yt_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `publish_date` datetime NOT NULL,
  `thumbnail_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `album` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` timestamp NULL DEFAULT current_timestamp(),
  `download_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `download_started` datetime DEFAULT NULL,
  `download_completed` datetime DEFAULT NULL,
  `transcode_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transcode_started` datetime DEFAULT NULL,
  `transcode_completed` datetime DEFAULT NULL,
  `metaupdate_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metaupdate_started` datetime DEFAULT NULL,
  `metaupdate_completed` datetime DEFAULT NULL,
  `thumbnail_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail_started` datetime DEFAULT NULL,
  `thumbnail_completed` datetime DEFAULT NULL,
  `long_term` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `publish_programs`
-- (See below for the actual view)
--
CREATE TABLE `publish_programs` (
`id` int(10) unsigned
,`channel_name` varchar(255)
,`yt_id` varchar(255)
,`title` varchar(255)
,`publish_date` datetime
,`thumbnail_url` varchar(255)
,`album` varchar(255)
,`author` varchar(255)
,`status` varchar(255)
,`created` timestamp
,`download_status` varchar(255)
,`download_started` datetime
,`download_completed` datetime
,`transcode_status` varchar(255)
,`transcode_started` datetime
,`transcode_completed` datetime
,`metaupdate_status` varchar(255)
,`metaupdate_started` datetime
,`metaupdate_completed` datetime
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `publish_program_cnt`
-- (See below for the actual view)
--
CREATE TABLE `publish_program_cnt` (
`cnt` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `reactivate`
--

CREATE TABLE `reactivate` (
  `yt_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'waiting',
  `downloaded_on` datetime DEFAULT NULL,
  `transcoded_on` datetime DEFAULT NULL,
  `metaupdated_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `syncs`
--

CREATE TABLE `syncs` (
  `id` int(11) NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `yt_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'OK'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `transcode-error`
-- (See below for the actual view)
--
CREATE TABLE `transcode-error` (
`id` int(10) unsigned
,`channel_name` varchar(255)
,`yt_id` varchar(255)
,`title` varchar(255)
,`publish_date` datetime
,`thumbnail_url` varchar(255)
,`album` varchar(255)
,`author` varchar(255)
,`status` varchar(255)
,`created` timestamp
,`download_status` varchar(255)
,`download_started` datetime
,`download_completed` datetime
,`transcode_status` varchar(255)
,`transcode_started` datetime
,`transcode_completed` datetime
,`metaupdate_status` varchar(255)
,`metaupdate_started` datetime
,`metaupdate_completed` datetime
);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `passwd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `white_lists`
--

CREATE TABLE `white_lists` (
  `id` int(10) UNSIGNED NOT NULL,
  `channel_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_contain` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT 10,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '140',
  `convert_to_mono` tinyint(1) NOT NULL DEFAULT 1,
  `album` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `long_term` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `white_lists`
--

INSERT INTO `white_lists` (`id`, `channel_name`, `title_contain`, `priority`, `type`, `convert_to_mono`, `album`, `author`, `long_term`) VALUES
(1, '美国之音中文网', NULL, 10, '140', 1, '美国之音中文网', '美国之音', 0),
(2, 'BBC News 中文', NULL, 10, '140', 1, 'BBC News 中文', 'BBC News', 0),
(3, '文昭談古論今 - Wen Zhao Official', NULL, 10, '140', 1, '文昭談古論今 - Wen Zhao Official', '文昭', 0),
(4, '江峰时刻', NULL, 10, '140', 1, '江峰时刻', '江峰', 0),
(5, '民視讚夯 Formosa TV Thumbs Up', NULL, 10, '140', 1, '民視讚夯 Formosa TV Thumbs Up', '民視', 0),
(6, '洛杉矶华人资讯网How视频', NULL, 10, '140', 1, '洛杉矶华人资讯网How视频', 'How视频', 0),
(7, '天亮時分', NULL, 10, '140', 1, '天亮時分', '章天亮', 0),
(8, '王剑每日观察Kim\'s Observation', NULL, 10, '140', 1, '王剑每日观察Kim\'s Observation', '王剑', 0),
(9, '明鏡火拍', NULL, 10, '140', 1, '明鏡火拍', '明镜电视家', 0),
(10, '阿波羅新聞網 - 熱點直擊', NULL, 10, '140', 1, '阿波羅新聞網 - 熱點直擊', '阿波羅新聞網', 0),
(11, '公子時評', NULL, 10, '140', 1, '公子時評', '公子沈', 0),
(12, '華爾街電視', NULL, 10, '140', 1, '華爾街電視', '華爾街電視', 0),
(13, '希望之聲TV', NULL, 10, '140', 1, '希望之聲TV', '希望之聲', 0),
(14, '真观点', NULL, 10, '140', 1, '真观点', '真飛', 0),
(15, '老北京茶馆', NULL, 10, '140', 1, '老北京茶馆', '老北京茶馆', 0),
(16, '明镜新闻台', NULL, 10, '140', 1, '明镜新闻台', '明镜电视家', 0),
(17, '大康有话说', NULL, 10, '140', 1, '大康有话说', '大康', 0),
(18, 'DW 中文 - 德國之聲', NULL, 10, '140', 1, 'DW 中文 - 德國之聲', '德國之聲', 0),
(19, 'LT視界', NULL, 10, '140', 1, 'LT視界', 'LT視界', 0);

-- --------------------------------------------------------

--
-- Structure for view `album_cnt`
--
DROP TABLE IF EXISTS `album_cnt`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `album_cnt`  AS  select `programs`.`album` AS `album`,count(0) AS `cnt` from `programs` where `programs`.`album` is not null and `programs`.`transcode_status` = 'OK' group by `programs`.`album` order by count(0) desc ;

-- --------------------------------------------------------

--
-- Structure for view `author_cnt`
--
DROP TABLE IF EXISTS `author_cnt`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `author_cnt`  AS  select `programs`.`author` AS `author`,count(0) AS `cnt` from `programs` where `programs`.`author` is not null and `programs`.`transcode_status` = 'OK' group by `programs`.`author` order by count(0) desc ;

-- --------------------------------------------------------

--
-- Structure for view `download-error`
--
DROP TABLE IF EXISTS `download-error`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `download-error`  AS  select `programs`.`id` AS `id`,`programs`.`channel_name` AS `channel_name`,`programs`.`yt_id` AS `yt_id`,`programs`.`title` AS `title`,`programs`.`publish_date` AS `publish_date`,`programs`.`thumbnail_url` AS `thumbnail_url`,`programs`.`album` AS `album`,`programs`.`author` AS `author`,`programs`.`status` AS `status`,`programs`.`created` AS `created`,`programs`.`download_status` AS `download_status`,`programs`.`download_started` AS `download_started`,`programs`.`download_completed` AS `download_completed`,`programs`.`transcode_status` AS `transcode_status`,`programs`.`transcode_started` AS `transcode_started`,`programs`.`transcode_completed` AS `transcode_completed`,`programs`.`metaupdate_status` AS `metaupdate_status`,`programs`.`metaupdate_started` AS `metaupdate_started`,`programs`.`metaupdate_completed` AS `metaupdate_completed` from `programs` where `programs`.`status` = 'can_download' and (`programs`.`download_status` is null or `programs`.`download_status` <> 'OK' and `programs`.`download_status` <> 'ignore') order by `programs`.`publish_date` desc ;

-- --------------------------------------------------------

--
-- Structure for view `download_time`
--
DROP TABLE IF EXISTS `download_time`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `download_time`  AS  select `programs`.`yt_id` AS `yt_id`,`programs`.`author` AS `author`,`programs`.`title` AS `title`,`programs`.`download_completed` - `programs`.`download_started` AS `download_time`,`programs`.`download_completed` AS `download_completed` from `programs` order by `programs`.`download_completed` desc ;

-- --------------------------------------------------------

--
-- Structure for view `publish_programs`
--
DROP TABLE IF EXISTS `publish_programs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `publish_programs`  AS  select `programs`.`id` AS `id`,`programs`.`channel_name` AS `channel_name`,`programs`.`yt_id` AS `yt_id`,`programs`.`title` AS `title`,`programs`.`publish_date` AS `publish_date`,`programs`.`thumbnail_url` AS `thumbnail_url`,`programs`.`album` AS `album`,`programs`.`author` AS `author`,`programs`.`status` AS `status`,`programs`.`created` AS `created`,`programs`.`download_status` AS `download_status`,`programs`.`download_started` AS `download_started`,`programs`.`download_completed` AS `download_completed`,`programs`.`transcode_status` AS `transcode_status`,`programs`.`transcode_started` AS `transcode_started`,`programs`.`transcode_completed` AS `transcode_completed`,`programs`.`metaupdate_status` AS `metaupdate_status`,`programs`.`metaupdate_started` AS `metaupdate_started`,`programs`.`metaupdate_completed` AS `metaupdate_completed` from `programs` where `programs`.`transcode_status` like 'OK' order by `programs`.`download_completed` desc ;

-- --------------------------------------------------------

--
-- Structure for view `publish_program_cnt`
--
DROP TABLE IF EXISTS `publish_program_cnt`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `publish_program_cnt`  AS  select count(0) AS `cnt` from `publish_programs` where `publish_programs`.`transcode_status` = 'OK' ;

-- --------------------------------------------------------

--
-- Structure for view `transcode-error`
--
DROP TABLE IF EXISTS `transcode-error`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `transcode-error`  AS  select `programs`.`id` AS `id`,`programs`.`channel_name` AS `channel_name`,`programs`.`yt_id` AS `yt_id`,`programs`.`title` AS `title`,`programs`.`publish_date` AS `publish_date`,`programs`.`thumbnail_url` AS `thumbnail_url`,`programs`.`album` AS `album`,`programs`.`author` AS `author`,`programs`.`status` AS `status`,`programs`.`created` AS `created`,`programs`.`download_status` AS `download_status`,`programs`.`download_started` AS `download_started`,`programs`.`download_completed` AS `download_completed`,`programs`.`transcode_status` AS `transcode_status`,`programs`.`transcode_started` AS `transcode_started`,`programs`.`transcode_completed` AS `transcode_completed`,`programs`.`metaupdate_status` AS `metaupdate_status`,`programs`.`metaupdate_started` AS `metaupdate_started`,`programs`.`metaupdate_completed` AS `metaupdate_completed` from `programs` where `programs`.`download_status` = 'OK' and `programs`.`transcode_status` <> 'OK' and `programs`.`transcode_status` <> 'retired' ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `channels`
--
ALTER TABLE `channels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `youtube_id_2` (`youtube_id`),
  ADD KEY `youtube_id` (`youtube_id`);

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `yt_id` (`yt_id`),
  ADD KEY `publish_date` (`publish_date`),
  ADD KEY `channel_name` (`channel_name`(191)),
  ADD KEY `title` (`title`(191)),
  ADD KEY `download_status` (`download_status`),
  ADD KEY `download_completed` (`download_completed`),
  ADD KEY `transcode_status` (`transcode_status`),
  ADD KEY `status` (`status`),
  ADD KEY `album` (`album`,`author`),
  ADD KEY `long_term` (`long_term`);

--
-- Indexes for table `reactivate`
--
ALTER TABLE `reactivate`
  ADD PRIMARY KEY (`yt_id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `syncs`
--
ALTER TABLE `syncs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`),
  ADD KEY `yt_id` (`yt_id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`) USING BTREE;

--
-- Indexes for table `white_lists`
--
ALTER TABLE `white_lists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `channel` (`channel_name`(191)),
  ADD KEY `title` (`title_contain`(191)),
  ADD KEY `priority` (`priority`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `channels`
--
ALTER TABLE `channels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `syncs`
--
ALTER TABLE `syncs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `white_lists`
--
ALTER TABLE `white_lists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
