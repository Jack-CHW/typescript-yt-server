import * as services from "../src/services/users"

import pkg from "mariadb"
const mariadb = pkg

const mockQuery = jest.fn(() => [])
jest.mock("mariadb", () => ({
  createPool: () => ({
    getConnection: () => ({
      query: mockQuery,
      release: jest.fn(),
    }),
  }),
}))

describe("test contranct pdf generation", () => {
  beforeEach(() => {
    mockQuery.mockClear()
  })
  it("should request db", async () => {
    const username = "testUserName"
    const result = await services.isUnique(username)
    expect(mockQuery.mock.calls.length).toBe(1)
    expect(mockQuery).toHaveBeenCalledWith(
      "SELECT * FROM users WHERE username=?",
      [username]
    )
  })

  it("should get all users", async () => {
    const result = await services.getUsers()
    expect(mockQuery.mock.calls.length).toBe(1)
    expect(mockQuery).toHaveBeenCalledWith("SELECT * FROM users", undefined)
  })

  it("should get one users", async () => {
    const username = "testUserName"
    const result = await services.getUsers(username)
    expect(mockQuery.mock.calls.length).toBe(1)
    expect(mockQuery).toHaveBeenCalledWith(
      "SELECT * FROM users WHERE username=?",
      [username]
    )
  })

  it("should create one user", async () => {
    const username = "testUserName"
    const passwd = "abc"
    const result = await services.createUser(username, passwd)
    expect(mockQuery.mock.calls.length).toBe(1)
  })

  it("should delete one user", async () => {
    const username = "testUserName"
    const result = await services.deleteUser(username)
    expect(mockQuery.mock.calls.length).toBe(1)
    expect(mockQuery).toHaveBeenCalledWith(
      "DELETE FROM users WHERE username=?",
      [username]
    )
  })
})
