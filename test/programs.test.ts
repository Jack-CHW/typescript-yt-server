import * as services from "../src/services/programs"

import pkg from "mariadb"
const mariadb = pkg

const mockQuery = jest.fn(() => [])
jest.mock("mariadb", () => ({
  createPool: () => ({
    getConnection: () => ({
      query: mockQuery,
      release: jest.fn(),
    }),
  }),
}))

describe("get one program", () => {
  beforeEach(() => {
    mockQuery.mockClear()
  })
  it("should get all users", async () => {
    const yt_id = "fwefewfw"
    const result = await services.getProgram(yt_id)
    expect(mockQuery.mock.calls.length).toBe(1)
    expect(mockQuery).toHaveBeenCalledWith(
      "select yt_id, publish_date, download_completed as download_date, title, album, author, transcode_status as status, thumbnail_url from programs where yt_id = ?",
      [yt_id]
    )
  })
})

describe("get all programs", () => {
  beforeEach(() => {
    mockQuery.mockClear()
  })

  it("should get programs of an author", async () => {
    const conditions = {
      per_page: 20,
      page: 1,
      author: "蕭若元",
      album: "",
      title: "",
      search: "",
    }
    const sql =
      "SELECT yt_id, publish_date, download_completed as download_date, title, album, author, transcode_status as status, thumbnail_url FROM programs WHERE author = ? ORDER BY download_completed desc LIMIT ?,?"
    const result = await services.searchPrograms(conditions)
    expect(mockQuery.mock.calls.length).toBe(1)
    expect(mockQuery).toHaveBeenCalledWith(sql, ["蕭若元", 0, 20])
  })

  it("should get programs of an album", async () => {
    const conditions = {
      per_page: 20,
      page: 3,
      author: "",
      album: "悶透社",
      title: "",
      search: "",
    }
    const sql =
      "SELECT yt_id, publish_date, download_completed as download_date, title, album, author, transcode_status as status, thumbnail_url FROM programs WHERE album = ? ORDER BY download_completed desc LIMIT ?,?"
    const result = await services.searchPrograms(conditions)
    expect(mockQuery.mock.calls.length).toBe(1)
    expect(mockQuery).toHaveBeenCalledWith(sql, ["悶透社", 40, 20])
  })

  it("should get programs of a title", async () => {
    const conditions = {
      per_page: 5,
      page: 4,
      author: "",
      album: "",
      title: "this is a title",
      search: "",
    }
    const sql =
      "SELECT yt_id, publish_date, download_completed as download_date, title, album, author, transcode_status as status, thumbnail_url FROM programs WHERE title = ? ORDER BY download_completed desc LIMIT ?,?"
    const result = await services.searchPrograms(conditions)
    expect(mockQuery.mock.calls.length).toBe(1)
    expect(mockQuery).toHaveBeenCalledWith(sql, ["this is a title", 15, 5])
  })

  it("should get programs of an album and a title", async () => {
    const conditions = {
      per_page: 10,
      page: 4,
      author: "",
      album: "悶透社",
      title: "a tested title",
      search: "",
    }
    const sql =
      "SELECT yt_id, publish_date, download_completed as download_date, title, album, author, transcode_status as status, thumbnail_url FROM programs WHERE album = ? AND title = ? ORDER BY download_completed desc LIMIT ?,?"
    const result = await services.searchPrograms(conditions)
    expect(mockQuery.mock.calls.length).toBe(1)
    expect(mockQuery).toHaveBeenCalledWith(sql, [
      "悶透社",
      "a tested title",
      30,
      10,
    ])
  })

  it("should get programs of an title", async () => {
    const conditions = {
      per_page: 10,
      page: 4,
      author: "",
      album: "悶透社",
      title: "a tested title",
      search: "",
    }
    const sql =
      "SELECT yt_id, publish_date, download_completed as download_date, title, album, author, transcode_status as status, thumbnail_url FROM programs WHERE album = ? AND title = ? ORDER BY download_completed desc LIMIT ?,?"
    const result = await services.searchPrograms(conditions)
    expect(mockQuery.mock.calls.length).toBe(1)
    expect(mockQuery).toHaveBeenCalledWith(sql, [
      "悶透社",
      "a tested title",
      30,
      10,
    ])
  })

  it("should get programs with search field", async () => {
    const conditions = {
      per_page: 10,
      page: 4,
      author: "",
      album: "",
      title: "",
      search: "this is a test",
    }
    const sql =
      "SELECT yt_id, publish_date, download_completed as download_date, title, album, author, transcode_status as status, thumbnail_url FROM programs WHERE CONCAT(title, album, author) LIKE ? ORDER BY download_completed desc LIMIT ?,?"
    const result = await services.searchPrograms(conditions)
    expect(mockQuery.mock.calls.length).toBe(1)
    expect(mockQuery).toHaveBeenCalledWith(sql, ["%this is a test%", 30, 10])
  })

  it("should get programs with search field and author", async () => {
    const conditions = {
      per_page: 10,
      page: 4,
      author: "蕭若元",
      album: "",
      title: "",
      search: "this is a test",
    }
    const sql =
      "SELECT yt_id, publish_date, download_completed as download_date, title, album, author, transcode_status as status, thumbnail_url FROM programs WHERE author = ? AND CONCAT(title, album, author) LIKE ? ORDER BY download_completed desc LIMIT ?,?"
    const result = await services.searchPrograms(conditions)
    expect(mockQuery.mock.calls.length).toBe(1)
    expect(mockQuery).toHaveBeenCalledWith(sql, [
      "蕭若元",
      "%this is a test%",
      30,
      10,
    ])
  })

  it("should get programs with wrong page", async () => {
    const conditions = {
      per_page: 3,
      page: -5,
      author: "",
      album: "",
      title: "",
      search: "",
    }
    const sql =
      "SELECT yt_id, publish_date, download_completed as download_date, title, album, author, transcode_status as status, thumbnail_url FROM programs ORDER BY download_completed desc LIMIT ?,?"
    const result = await services.searchPrograms(conditions)
    expect(mockQuery.mock.calls.length).toBe(1)
    expect(mockQuery).toHaveBeenCalledWith(sql, [0, 3])
  })

  it("should get programs with wrong page and per_page", async () => {
    const conditions = {
      per_page: -6,
      page: -5,
      author: "",
      album: "",
      title: "",
      search: "",
    }
    const sql =
      "SELECT yt_id, publish_date, download_completed as download_date, title, album, author, transcode_status as status, thumbnail_url FROM programs ORDER BY download_completed desc LIMIT ?,?"
    const result = await services.searchPrograms(conditions)
    expect(mockQuery.mock.calls.length).toBe(1)
    expect(mockQuery).toHaveBeenCalledWith(sql, [0, 20])
  })
})
