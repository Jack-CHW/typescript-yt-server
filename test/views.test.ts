import * as services from "../src/services/views"

import pkg from "mariadb"
const mariadb = pkg

const mockQuery = jest.fn(() => [])
jest.mock("mariadb", () => ({
  createPool: () => ({
    getConnection: () => ({
      query: mockQuery,
      release: jest.fn(),
    }),
  }),
}))

describe("get views", () => {
  beforeEach(() => {
    mockQuery.mockClear()
  })

  it("should get author views", async () => {
    const result = await services.getAuthors()
    expect(mockQuery.mock.calls.length).toBe(1)
    expect(mockQuery).toHaveBeenCalledWith(
      "SELECT * FROM author_cnt",
      undefined
    )
  })

  it("should get album views", async () => {
    const result = await services.getAlbum()
    expect(mockQuery.mock.calls.length).toBe(1)
    expect(mockQuery).toHaveBeenCalledWith("SELECT * FROM album_cnt", undefined)
  })
})
