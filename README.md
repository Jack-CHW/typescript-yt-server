# typescript yt server

A Typescript Server that works with https://bitbucket.org/852max/raspberry-pi-youtube-grasp/src/grasp-docker/

It is forked from https://github.com/jacKchw/cloud-learning-ts

## Run in Docker

Clone the project

```bash
  git clone git@bitbucket.org:Jack-CHW/typescript-yt-server.git
```

Go to the project directory

```bash
  cd typescript-yt-server
```

Update docker images

```bash
  docker-compose pull
```

Update container

```bash
  docker-compose up -d
```

## Run Locally

Create a grasp image

Please refer to readme in https://bitbucket.org/852max/raspberry-pi-youtube-grasp/src/grasp-docker/.

Clone the project

```bash
  git clone git@bitbucket.org:Jack-CHW/typescript-yt-server.git
```

Go to the project directory

```bash
  cd typescript-yt-server
```

Compile the build

```bash
  npm install
  npm run build
```

Build image locally

```bash
docker build -t jackchan56220/typescript-yt-server:latest .
```

Run docker-compose

```bash
  docker compose up -d
```

Open a brwoser and visit http://localhost:8080/ to access database through phpmyadmin

Open postman and call api via http://localhost:3000/

## Deploy to docker hub

Compile the build

```bash
  npm install
  npm run build
```

Build multi-arch image and publish

```bash
  docker buildx build --platform linux/arm64,linux/amd64,linux/arm/v7 -t jackchan56220/typescript-yt-server:latest -t jackchan56220/typescript-yt-server:1 --push .
```

## API Reference

https://app.swaggerhub.com/apis/jack-chw/typescript-yt-server/1.0.0
