import express from "express"
import logger from "morgan"
import passport from "passport"
import createError from "http-errors"
import cors from "cors"
import cookieParser from "cookie-parser"

import { usePassport, authenticate } from "./services/auth"
import userRoutes from "./routes/users"
import apimRoutes from "./routes/api"
import whiteListRoutes from "./routes/whiteList"
import downloadRoutes from "./routes/downloads"
import { initDB } from "./services/db"

initDB()

var app = express()

app.use(logger("dev"))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

const corsOptions = {
  origin: process.env.ORIGIN || "http://localhost",
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
  credentials: true,
}
app.use(cors(corsOptions))

app.use(passport.initialize())
usePassport()

app.use("/users", userRoutes)
app.use("/api", apimRoutes)
// app.use("/white-list", whiteListRoutes)
app.use("/download", downloadRoutes)

app.use("/thumbnail", authenticate, express.static("/app/thumbnail"))

// catch 404 and forward to error handler
app.use(function (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  next(createError(404))
})

// error handler
app.use(function (
  err: createError.HttpError,
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get("env") === "development" ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.json({ error: err.message })
})

export default app
