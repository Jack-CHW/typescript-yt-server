import { queryDB, InsertResult } from "../services/db"

export interface WhiteList {
  id: number
  channel_name: string
  title_contain: string | null
  album: string | null
  author: string | null
  type: string
  priority: number
  convert_to_mono: number
  long_term: number
}

export const getWhiteLists = async (): Promise<WhiteList[]> => {
  const query = "SELECT * FROM white_lists"
  return queryDB(query)
}

export const getWhiteList = async (
  id: number
): Promise<WhiteList | undefined> => {
  const query = "SELECT * FROM white_lists WHERE id = ?"
  const whiteLists = await queryDB(query, [id])
  return whiteLists[0]
}

export const createWhiteList = async (whiteList: WhiteList) => {
  const values = [
    null,
    whiteList.channel_name,
    whiteList.title_contain,
    whiteList.priority,
    whiteList.type,
    whiteList.convert_to_mono,
    whiteList.album,
    whiteList.author,
    whiteList.long_term,
  ]

  const query =
    "INSERT INTO white_lists(id, channel_name, title_contain, priority, type, convert_to_mono, album, author, long_term) VALUES (?,?,?,?,?,?,?,?,?)"
  const result = (await queryDB(query, values)) as InsertResult
  return result.insertId
}

export const editWhiteList = async (whiteList: WhiteList) => {
  const values = [
    whiteList.channel_name,
    whiteList.title_contain,
    whiteList.priority,
    whiteList.type,
    whiteList.convert_to_mono,
    whiteList.album,
    whiteList.author,
    whiteList.long_term,
    whiteList.id,
  ]

  const query =
    "UPDATE white_lists SET channel_name= ? ,title_contain= ? ,priority= ? ,type= ? ,convert_to_mono= ? ,album= ? ,author= ? ,long_term= ? WHERE id = ?"
  const result = await queryDB(query, values)
  return result
}

export const deleteWhiteList = async (id: number): Promise<WhiteList> => {
  const query = "DELETE FROM white_lists WHERE id = ?"
  const result = await queryDB(query, [id])
  return result
}
