import passport from "passport"
import { Strategy } from "passport-jwt"
import { Request } from "express"

var cookieExtractor = function (req: Request) {
  var token = null
  if (req && req.cookies) {
    token = req.cookies["jwt"]
  }
  return token
}

var opts = {
  jwtFromRequest: cookieExtractor,
  secretOrKey: process.env.SECRET || "abc",
}

interface JWT_Payload {
  username: string
}
export interface AuthenticatedRequest extends Request {
  user: JWT_Payload
}

/**
 * Inisiate passport strategy.
 * Should be called after app.use(passport.initialize()) and before routes
 */
export const usePassport = () => {
  passport.use(
    new Strategy(opts, function (user: JWT_Payload, done) {
      try {
        if (user.username) {
          return done(null, user)
        }
        return done(null, false)
      } catch (error) {
        return done(error, false)
      }
    })
  )
}

export const authenticate = passport.authenticate("jwt", { session: false })
