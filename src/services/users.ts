import { queryDB } from "../services/db"
import bcrypt from "bcrypt"
import jwt from "jsonwebtoken"
import config from "../config.json"
import createHttpError from "http-errors"

interface User {
  username: string
  passwd: string
}

/**
 * Check if a username is used
 *
 * @param username  user id
 * @return true if that username exist
 */

export const isUnique = async (username: string) => {
  const query = "SELECT * FROM users WHERE username=?"
  const results = await queryDB(query, [username])
  return results.length === 0
}

/**
 * Get info of users
 *
 * @param username  id of the target user. If userid is not given, this function will return all users' info
 * @return arrary of user info
 */

export const getUsers = async (username?: string): Promise<User[]> => {
  if (username) {
    const query = "SELECT * FROM users WHERE username=?"
    return queryDB(query, [username])
  }
  return queryDB(`SELECT * FROM users`)
}

/**
 * Create a record in users table
 *
 * @param username  id of the target user. If userid is not given, this function will return all users' info
 * @param passwd  password. It is hashed before storing into database
 * @return mysql query result
 */

export const createUser = async (username: string, passwd: string) => {
  const crypted = await bcrypt.hash(passwd, config.passwordSaltRounds)
  const query = "INSERT INTO users (username, passwd) VALUES (?,?)"
  return queryDB(query, [username, crypted])
}

/**
 * Create token when login. The duration of token refers to config.json
 *
 * @param user  record from users table
 * @param reqPassword  password to be verified. Provided from user for login
 * @return token
 */
export const createJWT = async (user: User, reqPassword: string) => {
  // verify password
  const compareResult = await bcrypt.compare(reqPassword, user.passwd)
  if (!compareResult) {
    throw new createHttpError.Unauthorized("invalid username or password")
  }

  //create JWT
  const jwtPayload = { username: user.username }
  const secretOrKey = process.env.SECRET || "abc"
  const token = jwt.sign(jwtPayload, secretOrKey, {
    expiresIn: config.JWTexpiresIn,
  })
  return token
}

/**
 * Remove a user record
 *
 * @param username  user to be removed
 * @return mysql query result
 */
export const deleteUser = async (username: string) => {
  const query = "DELETE FROM users WHERE username=?"
  return queryDB(query, [username])
}
