import { queryDB } from "./db"

interface QueryCondition {
  per_page: number
  page: number
  author: string
  album: string
  title: string
  search: string
}

interface Program {
  yt_id: string
  publish_date: string
  download_date: string
  title: string
  album: string
  author: string
  status: string
  thumbnail_url: string
}

/**
 * Check if a username is used
 *
 * @param yt_id  youtube id
 * @return arrary of youtube video
 */

export const getProgram = async (
  yt_id: string
): Promise<Program | undefined> => {
  const query =
    "select yt_id, publish_date, download_completed as download_date, title, album, author, transcode_status as status, thumbnail_url from programs where yt_id = ? AND transcode_status = 'OK' AND thumbnail_status = 'OK'"

  const result = (await queryDB(query, [yt_id])) as Program[]
  return result[0]
}

/**
 * Check if a username is used
 *
 * @param username  user id
 * @return true if that username exist
 */

export const searchPrograms = async (
  conditions: QueryCondition
): Promise<Program[]> => {
  conditions.page =
    isNaN(conditions.page) || conditions.page < 1 ? 1 : conditions.page
  conditions.per_page =
    isNaN(conditions.per_page) || conditions.per_page < 1
      ? 20
      : conditions.per_page
  const values = []
  const conditionQueries = []
  if (conditions.author) {
    conditionQueries.push("author = ?")
    values.push(conditions.author)
  }
  if (conditions.album) {
    conditionQueries.push("album = ?")
    values.push(conditions.album)
  }
  if (conditions.title) {
    conditionQueries.push("title = ?")
    values.push(conditions.title)
  }
  if (conditions.search) {
    conditionQueries.push("CONCAT(title, album, author) LIKE ?")
    values.push("%" + conditions.search + "%")
  }
  let whereTxt = conditionQueries.join(" AND ")
  if (whereTxt) {
    whereTxt = " AND " + whereTxt
  }
  values.push((conditions.page - 1) * conditions.per_page) //limit offset
  values.push(conditions.per_page) //limit

  const query = `SELECT yt_id, publish_date, download_completed as download_date, title, album, author, transcode_status as status, thumbnail_url 
  FROM programs 
  WHERE transcode_status = 'OK' AND thumbnail_status = 'OK' 
  ${whereTxt} 
  ORDER BY publish_date desc LIMIT ?,?`

  return queryDB(query, values)
}

export const searchProgramsID = async (
  conditions: QueryCondition
): Promise<Program[]> => {
  conditions.page =
    isNaN(conditions.page) || conditions.page < 1 ? 1 : conditions.page
  conditions.per_page =
    isNaN(conditions.per_page) || conditions.per_page < 1
      ? 20
      : conditions.per_page
  const values = []
  const conditionQueries = []
  if (conditions.author) {
    conditionQueries.push("author = ?")
    values.push(conditions.author)
  }
  if (conditions.album) {
    conditionQueries.push("album = ?")
    values.push(conditions.album)
  }
  if (conditions.title) {
    conditionQueries.push("title = ?")
    values.push(conditions.title)
  }
  if (conditions.search) {
    conditionQueries.push("CONCAT(title, album, author) LIKE ?")
    values.push("%" + conditions.search + "%")
  }
  let whereTxt = conditionQueries.join(" AND ")
  if (whereTxt) {
    whereTxt = " AND " + whereTxt
  }
  values.push((conditions.page - 1) * conditions.per_page) //limit offset
  values.push(conditions.per_page) //limit

  const query = `SELECT yt_id, publish_date, transcode_status as status, thumbnail_url 
  FROM programs 
  WHERE transcode_status = 'OK' AND thumbnail_status = 'OK'
  ${whereTxt} 
  ORDER BY publish_date desc LIMIT ?,?`

  return queryDB(query, values)
}

export const getPrograms = async () => {
  const query = `SELECT yt_id FROM programs`

  const result = (await queryDB(query)) as { yt_id: string }[]
  return result.map((obj) => obj.yt_id)
}
