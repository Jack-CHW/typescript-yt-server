import pkg from "mariadb"
const mariadb = pkg
import { createUser } from "./users"

export interface InsertResult {
  insertId: number
  affectedRows: number
  warningStatus: number
}

const pool = mariadb.createPool({
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
  connectionLimit: 5,
})

const admin = {
  username: process.env.ROOT_USERNAME || "admin",
  passwd: process.env.ROOT_PASSWORD || "abc",
}

/**
 * Send query to the mysql database
 *
 * @param query  SQL query
 * @param values  values for placeholder '?'
 * @return query result
 */
export const queryDB = async (query: string, values?: any) => {
  let conn
  try {
    conn = await pool.getConnection()
    return conn.query(query, values)
  } catch (err) {
    throw err
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Create necessary tables in mysql
 */
export const initDB = async () => {
  //create tables if not exist
  const userPromise = queryDB(
    "CREATE TABLE IF NOT EXISTS users(username varchar(255) primary key, passwd varchar(255) not null);"
  )

  //create admin
  await userPromise
  const users = await queryDB("SELECT username FROM users")
  if (users.length == 0) {
    await createUser(admin.username, admin.passwd)
  }
}
