import { queryDB } from "./db"

interface Author {
  author: string
  cnt: number
}

interface Album {
  album: string
  cnt: number
}

export const getAuthors = async (): Promise<Author[]> => {
  return queryDB("SELECT * FROM author_cnt")
}
export const getAlbum = async (): Promise<Album[]> => {
  return queryDB("SELECT * FROM album_cnt")
}
