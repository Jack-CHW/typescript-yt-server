import { errWrapper } from "./errorHandler"
import createHttpError from "http-errors"

import * as services from "../services/whiteList"

const ER_INVALID_ID = new createHttpError.BadRequest("invalid id")
const ER_REQUIRE_ID = new createHttpError.BadRequest("missing id")
const ER_REQUIRE_PARAMS = new createHttpError.BadRequest("missing body params")

export const getWhiteLists = errWrapper(async (req, res, next) => {
  const whiteLists = await services.getWhiteLists()
  res.json({ data: whiteLists })
})

export const getWhiteList = errWrapper(async (req, res, next) => {
  const id = Number(req.params.id)
  if (isNaN(id)) {
    throw ER_REQUIRE_ID
  }
  const whiteList = await services.getWhiteList(id)

  if (!whiteList) {
    throw ER_INVALID_ID
  }
  res.json({ data: whiteList })
})

export const createWhiteList = errWrapper(async (req, res, next) => {
  const body = req.body
  const whiteList = {
    id: 0,
    channel_name: body.channel_name,
    title_contain: body.title_contain || null,
    album: body.album || null,
    author: body.author || null,
    priority: body.priority,
    type: body.type,
    convert_to_mono: body.convert_to_mono,
    long_term: body.long_term,
  } as services.WhiteList

  if (
    whiteList.channel_name == undefined ||
    whiteList.priority == undefined ||
    whiteList.type == undefined ||
    whiteList.convert_to_mono == undefined ||
    whiteList.long_term == undefined
  ) {
    throw ER_REQUIRE_PARAMS
  }

  if (whiteList.priority > 100) {
    whiteList.priority = 100
  }
  const result = await services.createWhiteList(whiteList)
  whiteList.id = result
  res.json({ data: whiteList })
})

export const editWhiteList = errWrapper(async (req, res, next) => {
  const id = Number(req.params.id)
  if (isNaN(id)) {
    throw ER_REQUIRE_ID
  }

  const body = req.body
  const whiteList = {
    id: id,
    channel_name: body.channel_name,
    title_contain: body.title_contain || null,
    album: body.album || null,
    author: body.author || null,
    priority: body.priority,
    type: body.type,
    convert_to_mono: body.convert_to_mono,
    long_term: body.long_term,
  } as services.WhiteList
  if (
    whiteList.channel_name == undefined ||
    whiteList.priority == undefined ||
    whiteList.type == undefined ||
    whiteList.convert_to_mono == undefined ||
    whiteList.long_term == undefined
  ) {
    throw ER_REQUIRE_PARAMS
  }

  if (whiteList.priority > 100) {
    whiteList.priority = 100
  }

  await services.editWhiteList(whiteList)
  res.json({ data: whiteList })
})

export const deleteWhiteList = errWrapper(async (req, res, next) => {
  const id = Number(req.params.id)
  if (isNaN(id)) {
    throw ER_REQUIRE_ID
  }
  await services.deleteWhiteList(id)
  res.json({ data: null })
})
