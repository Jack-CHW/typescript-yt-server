import createHttpError from "http-errors"
import { errWrapper, errWrapperAuth } from "./errorHandler"
import * as services from "../services/users"
import config from "../config.json"

const ER_REQUIRE_USERNAME = new createHttpError.BadRequest("missing username")
const ER_REQUIRE_PASSWORD = new createHttpError.BadRequest("missing password")

export const getUsers = errWrapper(async (req, res, next) => {
  const users = await services.getUsers()
  const data = users.map((user) => user.username)
  res.json({ data: data })
})

export const getUniqueUser = errWrapper(async (req, res, next) => {
  const username = req.params.username
  if (!username) {
    throw ER_REQUIRE_USERNAME
  }
  const unique = await services.isUnique(username)
  res.json({ data: unique })
})

export const signup = errWrapper(async (req, res, next) => {
  const body = req.body
  if (!body.username) {
    throw ER_REQUIRE_USERNAME
  }
  if (!body.passwd) {
    throw ER_REQUIRE_PASSWORD
  }
  //check if user exist
  const unique = await services.isUnique(body.username)
  if (!unique) {
    throw new createHttpError.Conflict("duplicate username")
  }

  await services.createUser(body.username, body.passwd)
  res.json({ data: { username: body.username } })
})

export const login = errWrapper(async (req, res, next) => {
  const body = req.body

  if (!body.username) {
    throw ER_REQUIRE_USERNAME
  }
  if (!body.passwd) {
    throw ER_REQUIRE_PASSWORD
  }

  const users = await services.getUsers(body.username)
  if (users.length == 0) {
    throw new createHttpError.Unauthorized("invalid username or password")
  }

  const token = await services.createJWT(users[0], body.passwd)
  res.cookie("jwt", token, {
    httpOnly: true,
    sameSite: true,
    maxAge: config.JWTexpiresIn * 1000,
  })

  res.json({ data: token })
})

export const deleteUser = errWrapperAuth(async (req, res, next) => {
  const username = req.params.username

  console.log(req.user, username)
  if (!username) {
    throw ER_REQUIRE_USERNAME
  }

  if (username != req.user.username) {
    throw new createHttpError.Unauthorized()
  }

  const results = await services.deleteUser(username)
  if (results.affectedRows === 0) {
    throw new createHttpError.BadRequest("no user found")
  }
  res.json({ data: username })
})
