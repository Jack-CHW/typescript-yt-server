import createHttpError from "http-errors"
import { errWrapper } from "./errorHandler"

import * as programServices from "../services/programs"
import * as viewServices from "../services/views"

export const getProgram = errWrapper(async (req, res, next) => {
  const yt_id = req.params.yt_id

  if (!yt_id) {
    throw new createHttpError.BadRequest("missing yt_id")
  }

  const program = await programServices.getProgram(yt_id)
  res.json({ data: program })
})

export const searchPrograms = errWrapper(async (req, res, next) => {
  const conditions = {
    per_page: Number(req.query.per_page),
    page: Number(req.query.page),
    author: req.query.author as string,
    album: req.query.album as string,
    title: req.query.title as string,
    search: req.query.search as string,
  }

  const programs = await programServices.searchPrograms(conditions)

  res.json({ data: programs })
})

export const searchProgramsID = errWrapper(async (req, res, next) => {
  const conditions = {
    per_page: Number(req.query.per_page),
    page: Number(req.query.page),
    author: req.query.author as string,
    album: req.query.album as string,
    title: req.query.title as string,
    search: req.query.search as string,
  }

  const programs = await programServices.searchProgramsID(conditions)

  res.json({ data: programs })
})

export const getPrograms = errWrapper(async (req, res, next) => {
  const programs = await programServices.getPrograms()
  res.json({ data: programs })
})

export const getAuthors = errWrapper(async (req, res, next) => {
  const authors = await viewServices.getAuthors()
  res.json({ data: authors })
})

export const getAlbums = errWrapper(async (req, res, next) => {
  const albums = await viewServices.getAlbum()
  res.json({ data: albums })
})

export const menu = errWrapper(async (req, res, next) => {
  // retrieve data from database
  const authors = viewServices.getAuthors()
  const albums = viewServices.getAlbum()

  // response body
  const data = {
    homepage: {
      name: "主頁",
      url: "/",
    },
    authors: {
      name: "講者",
      url: "#",
      menu_items: await authors,
    },
    albums: {
      name: "專輯",
      url: "#",
      menu_items: await albums,
    },
  }
  res.json({ data: data })
})
