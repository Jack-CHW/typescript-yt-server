import createHttpError from "http-errors"
import { errWrapper } from "./errorHandler"
import { getProgram } from "../services/programs"

export const downloadAudio = errWrapper(async (req, res, next) => {
  const yt_id = req.params.yt_id

  if (!yt_id) {
    throw new createHttpError.BadRequest("missing yt_id")
  }

  const program = await getProgram(yt_id)

  if (!program) {
    throw new createHttpError.BadRequest("invalid yt_id")
  }

  if (program.status != "OK") {
    throw new createHttpError.InternalServerError("download is not ready yet")
  }
  const date = new Date(program.publish_date)
  const fileName = `${program.title.slice(
    0,
    5
  )}_${date.getFullYear()}${date.getMonth()}${date.getDate()}_${date.toLocaleTimeString(
    undefined,
    { hour12: false }
  )}.m4a`

  res.download(`/app/transcodes/${yt_id}.m4a`, fileName)
})
