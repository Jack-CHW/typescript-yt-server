import { Request, Response, NextFunction } from "express"
import { AuthenticatedRequest } from "../services/auth"

export type RouterFunction<Type, Req> = (
  req: Req,
  res: Response,
  next?: NextFunction
) => Promise<Type>

//wrapper for error handling
export const errWrapper = (callback: RouterFunction<void, Request>) => {
  return (req: Request, res: Response, next: NextFunction) => {
    callback(req, res, next).catch(next)
  }
}

export const errWrapperAuth = (
  callback: RouterFunction<void, AuthenticatedRequest>
) => {
  return (req: AuthenticatedRequest, res: Response, next: NextFunction) => {
    callback(req, res, next).catch(next)
  }
}
