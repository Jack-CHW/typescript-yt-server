import { Router } from "express"
import { authenticate } from "../services/auth"

import * as controllers from "../controllers/downloads"

var router = Router()

router.get("/:yt_id", authenticate, controllers.downloadAudio)

export default router
