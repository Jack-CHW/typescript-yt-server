import { Router } from "express"
import { authenticate } from "../services/auth"

import * as controllers from "../controllers/api"

var router = Router()

router.get("/program/:yt_id", authenticate, controllers.getProgram)
router.get("/program", authenticate, controllers.getPrograms)

router.get("/author", controllers.getAuthors)
router.get("/album", controllers.getAlbums)

router.get("/search", authenticate, controllers.searchPrograms)
router.get("/searchID", authenticate, controllers.searchProgramsID)
router.get("/menu", controllers.menu)

export default router
