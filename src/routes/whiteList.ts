import { Router } from "express"
import { authenticate } from "../services/auth"

import * as controllers from "../controllers/whiteList"

var router = Router()

router.get("/", authenticate, controllers.getWhiteLists)

router.get("/:id", authenticate, controllers.getWhiteList)

router.post("/", authenticate, controllers.createWhiteList)

router.put("/:id", authenticate, controllers.editWhiteList)

router.delete("/:id", authenticate, controllers.deleteWhiteList)

export default router
