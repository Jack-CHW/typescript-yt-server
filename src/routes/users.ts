import { Router, Request } from "express"
import { authenticate } from "../services/auth"
import { RouterFunction } from "../controllers/errorHandler"

import * as controllers from "../controllers/users"

var router = Router()

router.get("/", authenticate, controllers.getUsers)

router.get("/unique/:username", authenticate, controllers.getUniqueUser)

router.post("/", authenticate, controllers.signup)

router.post("/login", controllers.login)

router.delete(
  "/:username",
  authenticate,
  controllers.deleteUser as RouterFunction<void, Request>
)

export default router
