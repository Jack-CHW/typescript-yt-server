## API

## **Get Menu**

get data for menu bar

- **URL**

  /api/menu:

- **Method**

  `GET`

- **Success Response**

  **Code:** 200

  **Content:**

  ```json
  {
    "data": {
      "homepage": {
        "name": "主頁",
        "url": "/"
      },
      "authors": {
        "name": "講者",
        "url": "#",
        "menu_items": [
          {
            "author": "蕭若元",
            "cnt": 10
          }
        ]
      },
      "albums": {
        "name": "專輯",
        "url": "#",
        "menu_items": [
          {
            "album": "蕭生未分類",
            "cnt": 10
          }
        ]
      }
    }
  }
  ```

- **Sample Call:**

  ```
  curl -X 'GET' \
  'https://virtserver.swaggerhub.com/jack-chw/typescript-yt-server/1.0.0/api/menu' \
  -H 'accept: application/json'
  ```

## **Search Program**

Search programs by keywords

- **URL**

  /api/program

- **Method**

  `GET`

- **URL Params**

  **Optional:**

  `per_page=[number]`

  `page=[number]`

  `author=[string]`

  `album=[string]`

  `title=[string]`

  `search=[string]`

- **Success Response**

  **Code:** 200

  **Content:**

  ```json
  {
    "data": [
      {
        "yt_id": "6107Ksq9O7I",
        "publish_date": "2021-12-20T21:00:02.000Z",
        "download_date": "2021-12-20T13:11:03.000Z",
        "title": "選舉完了 林鄭被召上京 又有人吹風指陳馮富珍會做特首？福爾摩蕭告訴你為何陳馮富珍沒機會！12月尾打風 比六月飛霜更罕有！ 2021-12-20",
        "album": "蕭生未分類",
        "author": "蕭若元",
        "status": "OK",
        "thumbnail_url": "https://i3.ytimg.com/vi/6107Ksq9O7I/hqdefault.jpg"
      }
    ]
  }
  ```

- **Sample Call:**

  <_Just a sample call to your endpoint in a runnable format ($.ajax call or a curl request) - this makes life easier and more predictable._>

## **Get Program Info**

get one program record

- **URL**

  /api/program/:yt_id

- **Method**

  `GET`

- **URL Params**

  **Required:**

  `yt_id=[string]`

- **Success Response**

  **Code:** 200

  **Content:**

  ```json
  {
    "data": [
      {
        "yt_id": "6107Ksq9O7I",
        "publish_date": "2021-12-20T21:00:02.000Z",
        "download_date": "2021-12-20T13:11:03.000Z",
        "title": "選舉完了 林鄭被召上京 又有人吹風指陳馮富珍會做特首？福爾摩蕭告訴你為何陳馮富珍沒機會！12月尾打風 比六月飛霜更罕有！ 2021-12-20",
        "album": "蕭生未分類",
        "author": "蕭若元",
        "status": "OK",
        "thumbnail_url": "https://i3.ytimg.com/vi/6107Ksq9O7I/hqdefault.jpg"
      }
    ]
  }
  ```

- **Error Response**

  **Code:** 400 BAD REQUEST

  **Content:** `{ "error" : "missing yt_id" }`

- **Sample Call:**

  <_Just a sample call to your endpoint in a runnable format ($.ajax call or a curl request) - this makes life easier and more predictable._>

## **Login**

return JWT if username and password are valid

- **URL**

  /users/login

- **Method**

  `POST`

- **Data Params**

  ```json
  {
    "username": "john doe",
    "passwd": "Abc123"
  }
  ```

- **Success Response**

  **Code:** 200

  **Content:**

  ```json
  {
    "data": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImpvaG4gZG9lIiwiaWF0IjoxNjQwNDUwNTEwLCJleHAiOjE2NDA2MjMzMTB9.rvBvwnPJWrpjuHpjIenhqvhAxDeBlVd7PDCxw9rRy7k"
  }
  ```

- **Error Response**

  **Code:** 401 Unauthorized

  **Content:** `{ "error" : "invalid username or password" }`

- **Sample Call:**

  <_Just a sample call to your endpoint in a runnable format ($.ajax call or a curl request) - this makes life easier and more predictable._>

## **Get All Users**

An admin user can get all admins' username

- **URL**

  /users

- **Method**
  `GET`

- **Authentication:**
  bearerAuth

- **Success Response**

  **Code:** 200

  **Content:**

  ```json
  {
    "data": ["username"]
  }
  ```

- **Error Response**

  **Code:** 401 Unauthorized

  **Content:** `{ "error" : "invalid username or password" }`

- **Sample Call:**

  <_Just a sample call to your endpoint in a runnable format ($.ajax call or a curl request) - this makes life easier and more predictable._>

## **bearerAuth**

Some APIs requires authentication. You can call POST /users/login to get the token.

- **type:** http
- **scheme:** bearer
- **bearerFormat:** JWT
